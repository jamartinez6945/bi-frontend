import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../clients.service';
import { IClient } from '../client';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  clientsList: IClient[] = [];

  constructor(    private clientService: ClientsService,
    ) { }

  ngOnInit() {
    this.clientService.query()
    .subscribe(res => {
      this.clientsList = res;
    });
  }

  addClientToSale(item){
    
  }

}
