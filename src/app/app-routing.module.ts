import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'bikes',
    loadChildren: () => import('./modules/bike/bike.module')
    .then(m => m.BikeModule )
  },
  {
    path: 'clients',
    loadChildren: () => import('./modules/clients/clients.module')
    .then(m => m.ClientsModule )
  },
  {
    path: 'sales',
    loadChildren: () => import('./modules/sales/sales.module')
    .then(m => m.SalesModule )
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
